<?xml version="1.0" ?>
<!--

     Copyright (c) 2020 - 2024 Henix, https://www.henix.fr

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <testsuite errors="0">
            <xsl:attribute name="name">
                <xsl:value-of select="//activity[@type='test-suite']/@testsuitename"/>
            </xsl:attribute>
            <xsl:attribute name="time">
                <xsl:value-of select="//activity[@type='test-suite']/@durationms*0.001"/>
            </xsl:attribute>
            <xsl:attribute name="tests">
                <xsl:value-of select="count(//activity[@type='test-case'])"/>
            </xsl:attribute>
            <xsl:attribute name="failures">
                <xsl:value-of select="count(//activity[@type='test-case' and @result='Failed'])"/>
            </xsl:attribute>
            <xsl:attribute name="skipped">
                <xsl:value-of select="count(//activity[@type='test-case' and @result='Ignored'])"/>
            </xsl:attribute>
            <xsl:for-each select="//activity[@type='test-case']">
                <testcase>
                    <xsl:attribute name="name">
                        <xsl:value-of select="@displayName"/>
                    </xsl:attribute>
                    <xsl:attribute name="time">
                        <xsl:value-of select="@durationms*0.001"/>
                    </xsl:attribute>
                    <xsl:if test="@result='Failed'">
                        <failure>
                            <xsl:attribute name="message">
                                <xsl:value-of select="activity[@type='test-module' and @result='Failed']/errormessage"/>
                            </xsl:attribute>
                        </failure>
                    </xsl:if>
                    <xsl:if test="@result='Ignored'">
                        <error/>
                    </xsl:if>
                </testcase>
            </xsl:for-each>
        </testsuite>
    </xsl:template>
</xsl:stylesheet>
