<?xml version="1.0" ?>
<!--

     Copyright (c) 2020 - 2024 Henix, https://www.henix.fr

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <testsuite>
            <xsl:attribute name="name">
                <xsl:value-of select="//testsuites/@name"/>
            </xsl:attribute>
            <xsl:attribute name="time">
                <xsl:value-of select="//testsuites/@time"/>
            </xsl:attribute>
            <xsl:attribute name="tests">
                <xsl:value-of select="//testsuites/@tests"/>
            </xsl:attribute>
            <xsl:attribute name="failures">
                <xsl:value-of select="//testsuites/@failures"/>
            </xsl:attribute>
            <xsl:attribute name="errors">
                <xsl:value-of select="//testsuites/@errors"/>
            </xsl:attribute>
            <xsl:attribute name="skipped">
                <xsl:value-of select="sum(//testsuites/testsuite/@skipped)"/>
            </xsl:attribute>
            <xsl:for-each select="//testsuites/testsuite/testcase">
                <testcase>
                    <xsl:attribute name="name">
                        <xsl:value-of select="concat(../@id, '/', @name)"/>
                    </xsl:attribute>
                    <xsl:attribute name="time">
                        <xsl:value-of select="@time"/>
                    </xsl:attribute>
                    <xsl:if test="./failure">
                        <failure>
                            <xsl:attribute name="message">
                                <xsl:value-of select="./failure/@message"/>
                            </xsl:attribute>
                        </failure>
                    </xsl:if>
                    <xsl:if test="./error">
                        <error>
                            <xsl:attribute name="message">
                                <xsl:value-of select="./error/@message"/>
                            </xsl:attribute>
                        </error>
                    </xsl:if>
                    <xsl:element name="system-out">
                        <xsl:value-of select="system-out/text()"/>
                    </xsl:element>
                </testcase>
            </xsl:for-each>
        </testsuite>
    </xsl:template>
</xsl:stylesheet>
