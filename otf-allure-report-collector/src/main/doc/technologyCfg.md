<!--

     Copyright (c) 2020 - 2023 Henix, henix.fr

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.

-->
#### Warning! This feature is broken in this Micronaut Service for the moment due to incompatibilities between the current implementation and GraalVM native image.
# How to add support for a test technology not natively supported by Allure

If the tool supports any kind of XML based reporting, you may set up Allure integration
for this technology using the following facility : TechnologyCfg.

This facility defines a binding between technologies and XSL transforms to produce compatible reports
that can be injected into Allure. This configuration is set up as follows :

*  The OTF-allure-report-collector has an optional `org.opentestfactory.allure.technologyconfiguration`
application property. 
   *  If this property isn't defined, no configuration is setup, and all reports
are injected into the allure tool without transform.
   *  If defined, it must be a valid URL to access the technology mapping model. 
      The file is expected to be JSON with the following format :

      ```json
      {
         "technologyList": [
           {
              "name":"dazzlingTech",
              "reportRegex":"[\\S\\s]*dazzling-results\\.xml",
              "stylesheet":"file:///opt/allure-collector/dazzlingTech.xslt"
           },
           {
              "name":"dullTech",
              "reportRegex":"[\\S\\s]*results\\.dull",
              "stylesheet":"file:///opt/allure-collector/dullTech.xslt"
           }
        ]
      }
      ```

      For each technology, the three following fields are defined :
      
      *  `name`: this name is used for loggin, to name the target technology in messages.
      *  `reportRegex`: this is a regular expression to recognize attachment names produced 
       by the target technology.
      *  `stylesheet`: this is a URL to access the XSLT that will produce an XML report
         in a supported format for integration into Allure. 
