/*
 *  Copyright (c) 2020 - 2024 Henix, https://www.henix.fr
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.attribute.PosixFilePermission

def hookDirectoryInGitRepository = new File(".git/hooks")
def preCommitHookFile = new File(hookDirectoryInGitRepository,"pre-commit")

def preCommitHookFileContentLines = []

if (preCommitHookFile.exists()) {
    preCommitHookFile.eachLine {
        line -> preCommitHookFileContentLines << line
    }
}

def preCommitHookFileCallsCustomScript = preCommitHookFileContentLines.find {
    it ==~ /^\s*\. src\/main\/scripts\/precommit.sh\s*/
}

def ensurePreCommitHookFileIsExecutable = {
    def preCommitHookPath = Paths.get(preCommitHookFile.toURI())

    def permissions = Files.getPosixFilePermissions(preCommitHookPath);
    permissions.add(PosixFilePermission.OWNER_EXECUTE)
    permissions.add(PosixFilePermission.GROUP_EXECUTE)
    permissions.add(PosixFilePermission.OTHERS_EXECUTE)

    Files.setPosixFilePermissions(preCommitHookPath, permissions);
} as Object

if (preCommitHookFileCallsCustomScript) {
    println "precommit hook found, nothing to do"
    ensurePreCommitHookFileIsExecutable()
    return
} else {
    println "no precommit hook, will add hook definition"
    def old = new File(hookDirectoryInGitRepository,"pre-commit.old")
    if (preCommitHookFile.exists()) {
        old.withWriter {
            writer ->
                preCommitHookFileContentLines.each {
                    writer.writeLine it
                }
        }
    }
    preCommitHookFile << ". src/main/scripts/precommit.sh\n"
    ensurePreCommitHookFileIsExecutable()
}
