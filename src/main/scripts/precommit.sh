#!/bin/sh
#
#  Copyright (c) 2020 - 2024 Henix, https://www.henix.fr
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

echo pre-commit checks

echo revision consistency check
if [ $(find . -type f -name pom.xml -exec grep -o "<revision>.*</revision>" {} \; | sort | uniq | wc -l) -eq 1 ]; then 
   echo "Good"
else
   echo Inconsistent
   find . -type f -name pom.xml -exec grep -o "<revision>.*</revision>" {} \; -print
   exit 1
fi

echo ...license check
log=$(mktemp)
mvn -o -DskipTests license:check license:check@Modified-by license:check@OriginalCopyright>$log
status=$?

if [ $status -ne 0 ]; then
	echo "*** Failed: some license headers are missing or non-conformant ***"
	grep Missing $log| cut -d"]" -f 2
	if [ $(grep Missing $log | wc -l) -eq 0 ]; then
		echo "*** No missing header found, see log in target/precommit.log"
		mkdir -p target
		mv $log target/precommit.log
	else
		echo "To draft license editing, please run the following:"
		echo mvn license:format license:format@Modified-by license:format@OriginalCopyright
		rm $log
	fi
	exit 1
fi

echo "All checked files have conforming headers"
rm $log

mvn -o -DskipTests spotless:check
status=$?

if [ $status -ne 0 ]; then
    echo Code format check failed. To draft formatting changes, please run the following:
    echo mvn spotless:apply
    exit 1
fi

echo "All checked files comply with the required code format."
