## OpenTestFactory Interpreter Service

### Extend parser configuration for surefire reports

Launch service with command `-Dcustom.parsers.file=/path/to/my_configuration_file`.
Configuration file should describe a list of parsers and conform the following example.

```yaml
- name: Parser1
  matchCases:
    - name: case1
      regex: ^(?<part1>[^#]+)#(?<part2>[^#]+)#(?<part3>[^#]+)#(?<part4>[^#]+)$
      xpath: //testsuite[@name="{part2}"]/testcase[@name="{part3}" and @classname="{part4}"]
    - name: case2
      regex: ^(?<part1>[^#]+)#(?<part2>[^#]+)#(?<part3>[^#]+)$
      xpath: //testsuite[@name="{part2}"]/testcase[@name="{part3}"]
      skippedStatus: success
      acceptedTechnologies:
      - mytechnology1
      acceptedMediaTypes:
      - application/vnd.opentestfactory.mytechnology1-surefire+xml
- name: Parser2
  matchCases:
  - name: case 1
    regex: ^(?<part1AndPart2>[^#]+)#(?<part3>[^#]+)$
    xpath: //testsuite/testcase[contains(@name, "{part3}")]
    acceptedTechnologies:
      - mytechnology2
    acceptedMediaTypes:
      - application/vnd.opentestfactory.mytechnology2-surefire+xml
```
Each reported group in xpath must be written between quotes and braces and only one group should appear between quotes.  
Use 'concat' if you need two groups in the same attribute: `testcase[@name=concat("{part2}", "$", "{part3}")]`.

- skippedStatus: the output status when a test is skipped. Either "error" (default) or "success"
- acceptedTechnologies: an array of technologies accepted by the parser
- acceptedMediaTypes: an array of accepted media types (MIME types)

All properties are mandatory, except `skippedStatus` which defaults to 'error'. If the format is invalid or if a property is missing, then an exception will be thrown when starting the service.

Full internal configuration is available in file [internal-parsers.yml](src/main/resources/internal-parsers.yml).

Once started, configuration is read and each will be mapped by its technologies.

It is possible to override a parser for a given technology by configuring a custom parser declaring this given technology in `acceptedTechnologies` property. In this case, custom configuration prevails on internal configuration, if a technology is present for several custom configurations, then the last listed prevails.

When called, a parser will try to match the given test reference to each given regex in configuration in the same order they are declared.

When matched, the status is then computed using corresponding xpath. If no regex matches, then an error status is returned.
